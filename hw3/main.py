#宣告
import numpy as np
import cv2

#副函式對padding後圖片處理X和Y方向做處理，回傳X，Y結果值
def sobelfanfa(h, w, pad_img):
    # initial 存放X方向索貝爾結果
    X_img = np.zeros((h,w), dtype=np.uint8)
    # initial 存放Y方向索貝爾結果
    Y_img = np.zeros((h,w), dtype=np.uint8)
    # 走訪 整張圖片
    for x in range(h):
        for y in range(w):
            #X方向
            X_img[x, y] =abs(\
                (1 * pad_img[x, y]) + ((-1) * pad_img[x, y + 2]) + \
                (2 * pad_img[x + 1, y]) + ((-2) * pad_img[x + 1, y + 2]) + \
                (1 * pad_img[x + 2, y]) + ((-1) * pad_img[x + 2, y + 2])\
            )
            #Y方向
            Y_img[x, y] = abs(\
                (1 * pad_img[x, y]) + ((1) * pad_img[x, y + 2]) + \
                (2 * pad_img[x, y + 1]) + ((-2) * pad_img[x + 2, y + 1]) + \
                ((-1) * pad_img[x + 2, y]) + ((-1) * pad_img[x + 2, y + 2])\
            )
    # 已走訪整張圖
    return X_img , Y_img

#主程式開始
if __name__ == "__main__":
    #讀圖
    img = cv2.imread("./123.jpg", cv2.IMREAD_GRAYSCALE)
    #把圖縮小
    img = cv2.resize(img,(500,500))
    #用高斯模糊化 降噪
    img = cv2.GaussianBlur(img, (5, 5), 9)
    cv2.imshow("original gaussian after", img)

    #把圖使用映射的方式padding
    pad_img = np.pad(img, pad_width=((1, 1), (1, 1)),mode="reflect")

    #找出圖的長寬
    img_height, img_weight = img.shape
    print("origin size ", img_height, img_weight)


    # 新建一個X的空圖片
    X_img = np.zeros([img_height, img_weight, 1], dtype=np.uint8)
    # 把新的X的空圖都填入0
    X_img.fill(0)
    # 新建一個Y的空圖片
    Y_img = np.zeros([img_height, img_weight, 1], dtype=np.uint8)
    # 把新的Y的空圖都填入0
    Y_img.fill(0)

    # 新建一個新的圖片 ,uint 8  = Unsigned integer (0 to 255)
    sobel_img = np.zeros([img_height, img_weight, 1], dtype=np.uint8)
    sobel_img.fill(0)

    #把图用sobel方法做XY方向
    X_img , Y_img = sobelfanfa(img_height, img_weight,pad_img)

    cv2.imshow("x ",  X_img)
    cv2.imshow("y ",  Y_img)
    #cv2.imshow("x and y", np.hstack([X_img,Y_img]))

    #把sobelX和Y图做加权放到sobel的图里面
    sobel_img = cv2.addWeighted(Y_img, 0.5, X_img, 0.5, 1)

    cv2.imshow("sobel de tu", sobel_img)
    #把sobel的图跟原图做相加
    add_sobel = cv2.addWeighted(sobel_img, 0.5, img, 0.5, 1)
    #result
    cv2.imshow("result", add_sobel)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
