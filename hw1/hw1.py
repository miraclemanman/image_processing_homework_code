import cv2
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    # read a image from path
    image = cv2.imread("./123.jpg", cv2.IMREAD_COLOR)

    # convert image BGR to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # for debug. count picture total pixel
    print(gray.shape[0]*gray.shape[1])

    # load a image(gray) convert to numpy.ndarray
    img_array = np.array(gray)

    # 2D array flatten to 1D array
    oneDimList = img_array.flatten()

    # declare list
    listtY = [i for i in range(256)]
    listtX = [i for i in range(256)]

    # all set 0
    for j in listtY:
        listtY[j]=0

    # calculate all grayslace pixel
    for i in oneDimList:
        listtY[i]+=1

    # dictionary product to list  (useless)
    dictt = {e:listtY for e in range(256)}

    # for debug. image pixel確認大小有無不一樣
    print(listtY,sum(listtY))

    # make a 畫布
    fig = plt.figure()

    #set grayslace 位置
    fig.add_subplot(121)
    #show 圖片BGR to RGB 因為opencv and plt 讀的照片通道不一樣 會造成色偏
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB) )

    #set Histogram位置
    fig.add_subplot(122)
    # plot Bar graph
    plt.bar(listtX,listtY)

    # show figure
    plt.show()

    #cv2.imshow("Result:", gray)
    #cv2.waitKey(0)